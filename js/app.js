var TechieApp = angular.module('TechieApp', ['ngRoute','ui.bootstrap','ngAnimate']);

var CURR_IMAGE = 0;
var IMAGES = [ '1.jpg', '2.jpg', '3.jpg', '4.jpg'];
var changeImage = function(){
    //console.log(IMAGES[CURR_IMAGE%6]);
    $('.background-image').css('background', "url('../Techie/img/"+ IMAGES[CURR_IMAGE % 4] +"')");
    $('.background-image').css('background-position', 'center');
    $('.background-image').css('background-size', 'cover');
    CURR_IMAGE +=1;
}
TechieApp.controller('transitionCtrl', function(){
    setInterval(changeImage, 5000);
}); 

TechieApp.controller('CarouselDemoCtrl', function ($scope) {
  $scope.myInterval = 5000;
  $scope.noWrapSlides = false;
  $scope.active = 0;
  var slides = $scope.slides = [];
  var currIndex = 0;

  $scope.addSlide = function() {
    var newWidth = 600 + slides.length + 1;
    slides.push({
      image: ['img/1.jpg','img/2.jpg','img/3.jpg','img/4.jpg'][slides.length % 4],
      review: ['Only Desiigners can do this! Loved these guys. ','I like the way they think and work, Its always something out of the box.','That is so cool','We loved that from the moment we laid our eyes upon it, and knew that working with Desiigners was for the best. '][slides.length % 4],
      client: ['StartXUp Inc.', 'SomeNewFirm Inc.', 'AnotherClient Inc.', 'TestClient Inc.'] [slides.length % 4],
      id: currIndex++
    });
  };

  $scope.randomize = function() {
    var indexes = generateIndexesArray();
    assignNewIndexesToSlides(indexes);
  };

  for (var i = 0; i < 4; i++) {
    $scope.addSlide();
  }

  // Randomize logic below

  function assignNewIndexesToSlides(indexes) {
    for (var i = 0, l = slides.length; i < l; i++) {
      slides[i].id = indexes.pop();
    }
  }

  function generateIndexesArray() {
    var indexes = [];
    for (var i = 0; i < currIndex; ++i) {
      indexes[i] = i;
    }
    return shuffle(indexes);
  }

});

TechieApp.controller('ContactCtrl',function($scope,$http){
   $scope.pushdetails = function(client){
       $http({
           method: 'POST',
           url: 'php/pushdetails.php',
           data: client
       }).success(function(response){
          console.log(response); 
       });
       
   }
    
});

TechieApp.controller('WorkCtrl', function($scope){
    $scope.Images = [
        {
            src: '1.jpg'
        },
        {
            src: '2.jpg'
        },
        {
            src: '3.jpg'
        }
    ];
});

TechieApp.config(function($routeProvider){
   $routeProvider
   .when('/',{
       templateUrl: 'templates/home.html'
   })
   .when('/work',{
       templateUrl: 'templates/work.html',
       controller: 'WorkCtrl'
   })
   .when('/contact',{
       templateUrl: 'templates/contact.html',
       controller: 'ContactCtrl'
   })
   .when('/blog',{
       templateUrl: 'templates/blog.html'
   })
   .otherwise({
       templateUrl: 'templates/home.html'
   })
});

